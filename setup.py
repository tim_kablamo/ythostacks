from setuptools import setup

def get_version(file_name):
    with open(file_name, 'r') as f:
        version = f.read().strip()
    return version

setup(name='gostacks',
      version=get_version('ci/version.txt'),
      description='Python interface to the famous GoStacks AWS API and CloudFormation parser.',
      url='https://bitbucket.org/tim_kablamo/ythostacks',
      author='Tim Elson',
      author_email='tim.elson@kablamo.com',
      license='MIT',
      packages=['gostacks'],
      zip_safe=False)
