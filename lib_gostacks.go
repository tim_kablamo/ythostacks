package main

import (
	"C"
	"bitbucket.org/kablamo-dev/gostacks/tasks"
	"flag"
	"github.com/urfave/cli"
)

func getDummyContext(infile string) *cli.Context {
	set := flag.NewFlagSet("clib", 0)
	set.String("format", "yaml", "dummy flag")
	c := cli.NewContext(nil, set, nil)
	set.Parse([]string{"--format", "yaml", infile})
	return c
}

//export Generate
func Generate(infile string) {
	c := getDummyContext(infile)
	//c.Set("format", "nard")  // How to modify
	tasks.Generate(c)
}

func main() {}
