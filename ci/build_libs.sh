#!/usr/bin/env bash

export GOPATH=$(pwd)/go

# Have to put our go wrapper into the actual gostacks repo to get it to compile
ln -s $(pwd)/lib_gostacks.go ${GOPATH}/src/bitbucket.org/kablamo-dev/gostacks/lib_gostacks.go

platforms=( darwin windows linux )
for p in "${platforms[@]}"
do
    echo "Building for: ${p}"
    GOOS=${p} GOARCH=amd64;
    go build -v -o gostacks/lib/gostacks_${GOOS}_${GOARCH}.so -buildmode=c-shared ${GOPATH}/src/bitbucket.org/kablamo-dev/gostacks/lib_gostacks.go
done

# Remove our wrapper from the gostacks repo - May have it in permemnatly later
rm ${GOPATH}/src/bitbucket.org/kablamo-dev/gostacks/lib_gostacks.go

