# YThoStacks

Everyone else has one ...

Idea: Use `gostacks` as a python library

## Example

Open up a python shell and give it a spin ... currently only `generate` works.

You have to be in a directory with a `configs/` just like for `gostacks`. In the following example a file `configs/simpleec2.yaml` must exist

```python
>>> import gostacks.cloudformation as cfn
>>> cfn.generate('simpleec2')
generating cloudformation template from yaml
```

Observe the compiled template in `compiled/`

## Installation

```bash
git clone --recurse-submodules git@bitbucket.org:tim_kablamo/ythostacks.git
cd ythostacks
pip install .               # or sudo if you love filthy unpredictable machines
```
