from ctypes import cdll, c_longlong, c_double, c_void_p, c_char_p, Structure, POINTER
import platform
import os

here = os.path.dirname(os.path.realpath(__file__))

# TODO: Get archetecture and platform
arch = platform.architecture()[0]  # '64bit'
plat = platform.system() # "Darwin", 'Windows', 'Linux'

if arch != '64bit':
    raise EnvironmentError("Gostacks-python only supports 64bit systems at this stage. Your system is {}. Sorry ...".format(arch))

gostacks = cdll.LoadLibrary(os.path.join(here, "lib", "gostacks_{}_amd64.so".format(plat.lower())))

class GoString(Structure):
    """C type struct { const char *p; GoInt n; } """
    _fields_ = [("p", c_char_p), ("n", c_longlong)]


def generate(file_name):
    print("generating cloudformation template from yaml")

    gostacks.Generate.argtypes = [GoString]
    gostacks.Generate.restype = c_longlong

    # file_name = b"simpleec2"
    bstring = file_name.encode()
    msg = GoString(bstring, len(bstring))
    gostacks.Generate(msg)
